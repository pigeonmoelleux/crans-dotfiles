{ pkgs, ... }:

{
  imports = [
    ./git.nix
    ./helix
    ./neovim.nix
    ./tmux.nix
  ];

  home = rec {
    username = "_pigeonmoelleux";
    homeDirectory = "/home_nounou/pigeonmoelleux";
    stateVersion = "23.11";

    packages = with pkgs; [
      jq
      man-pages
      rclone
    ];
  };
}
