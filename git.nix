{ ... }:

{
  programs.git = {
    enable = true;
    lfs.enable = true;

    userName = "pigeonmoelleux";
    userEmail = "pigeonmoelleux@crans.org";

    extraConfig = {
      init = {
        defaultBranch = "main";
      };

      user = {
        signingKey = "1B91F0873D061319D3D07F91FA47BDA260489ADA";
      };

      core = {
        editor = "hx";
        excludefile = builtins.toFile ".gitignore_global" ''
          .vscode/
          .envrc
          .direnv/
        '';
      };

      advice = {
        addignoredfile = false;
      };

      filter.lfs = {
        required = true;
        clean = "git-lfs clean -- %f";
        smudge = "git-lfs smudge -- %f";
        process = "git-lfs filter-process";
      };

    };
  };
}
